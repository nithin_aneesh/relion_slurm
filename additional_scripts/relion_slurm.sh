#!/bin/bash
#SBATCH --ntasks=XXXmpinodesXXX
#SBATCH --partition=XXXqueueXXX
#SBATCH --cpus-per-task=XXXthreadsXXX
#SBATCH --error=XXXerrfileXXX
#SBATCH --output=XXXoutfileXXX
#SBATCH --open-mode=append
#SBATCH --mem=XXXMemoryXXX
#SBATCH -t XXXWallTimeXXX
#SBATCH -A XXXGrantnameXXX
#SBATCH --mail-type=ALL
#SBATCH --mail-user=XXXemailXXX
#SBATCH XXXNumberGPUsXXX
env | sort
module load plgrid/tools/relion/3.1.1
mpirun -np XXXmpinodesXXX XXXcommandXXX
